#!/bin/bash

hostnamectl set-hostname k8s-master


# 2.添加host
cat >> /etc/hosts << EOF
192.168.1.73 k8s-master
192.168.1.152 k8s-node1
192.168.1.245 k8s-node2
EOF


kubeadm init \
--control-plane-endpoint="k8s-master" \
--apiserver-advertise-address=192.168.1.73 \
--image-repository registry.aliyuncs.com/google_containers \
--kubernetes-version v1.25.2 \
--service-cidr=10.1.0.0/16 \
--pod-network-cidr=10.244.0.0/16 \
--cri-socket=unix:///var/run/cri-dockerd.sock \
--upload-certs \
--v=5

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl get nodes