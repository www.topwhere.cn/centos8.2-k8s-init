#!/bin/bash

# 1 更新资源包
yum update -y
sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
yum update -y
# 安装vim
yum -y install vim


# 安装时间同步
yum -y install chrony
systemctl start chronyd && systemctl enable chronyd
date



# 2 关闭防火墙
service firewalld stop && systemctl disable firewalld
#临时关闭selinux，永久关闭请修改/etc/selinux/config配置文件
setenforce 0

 # 3 关闭减缓空间
 # 临时关闭
swapoff -a
# getenforce查看状态
#永久关闭
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab


# 6.将桥接ipv6流量传递
# 开启 ipvs 模块
modprobe br_netfilter
#写入配置文件
cat <<EOF > /etc/sysctl.d/k8s.conf
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
fs.may_detach_mounts = 1
vm.overcommit_memory=1
vm.panic_on_oom=0
fs.inotify.max_user_watches=89100
fs.file-max=52706963
fs.nr_open=52706963
net.netfilter.nf_conntrack_max=2310720
EOF
cat > /etc/sysconfig/modules/ipvs.modules << EOF
#!/bin/sh
modprobe -- ip_vs
modprobe -- ip_vs_lc
modprobe -- ip_vs_wlc
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_lblc
modprobe -- ip_vs_lblcr
modprobe -- ip_vs_dh
modprobe -- ip_vs_sh
modprobe -- ip_vs_fo
modprobe -- ip_vs_nq
modprobe -- ip_vs_sed
modprobe -- ip_vs_ftp
modprobe -- nf_conntrack_ipv4
EOF
#生效配置文件
sysctl -p /etc/sysctl.d/k8s.conf
chmod 755 /etc/sysconfig/modules/ipvs.modules && bash /etc/sysconfig/modules/ipvs.modules && lsmod | grep -e ip_vs -e nf_conntrack_ipv4


# 7.下载docker
yum install -y yum-utils device-mapper-persistent-data lvm2
# 换源-这是官方的源
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# 换源-这是阿里的源
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
# 安装docker
yum -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin --allowerasing
# 生成配置文件
#curl -JLO https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-amd64-v1.1.1.tgz


mkdir -p /etc/cni/net.d /opt/cni/bin

tar xf cni-plugins-linux-amd64-v1.1.1.tgz -C /opt/cni/bin/

#curl -JLO https://github.com/containerd/containerd/releases/download/v1.6.6/cri-containerd-cni-1.6.6-linux-amd64.tar.gz
#解压
#tar -xzf cri-containerd-cni-1.6.8-linux-amd64.tar.gz -C /

#cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
#net.bridge.bridge-nf-call-iptables  = 1
#net.ipv4.ip_forward                 = 1
#net.bridge.bridge-nf-call-ip6tables = 1
#EOF
# 加载内核
#sysctl --system

containerd config default > /etc/containerd/config.toml

# 修改Containerd的配置文件
sed -i "s#SystemdCgroup\ \=\ false#SystemdCgroup\ \=\ true#g" /etc/containerd/config.toml
cat /etc/containerd/config.toml | grep SystemdCgroup

sed -i "s#k8s.gcr.io#registry.cn-hangzhou.aliyuncs.com/chenby#g" /etc/containerd/config.toml

sed -i "s#registry.k8s.io/pause:3.6#registry.aliyuncs.com/google_containers/pause:3.6#g"  /etc/containerd/config.toml

cat /etc/containerd/config.toml | grep sandbox_image

# 启动 containerd
systemctl daemon-reload && systemctl restart containerd && systemctl enable containerd



#启动 / #设置开机启动
systemctl start docker && systemctl enable docker.service

cat <<EOF > /etc/docker/daemon.json
{
  "registry-mirrors": ["https://y0753cg2.mirror.aliyuncs.com"],
    "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
#重启docker
systemctl restart docker



# 8. 加入k8s源
#cat <<EOF > /etc/yum.repos.d/kubernetes.repo
#[kubernetes]
#name=Kubernetes
#baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
#venabled=1
#gpgcheck=1
#repo_gpgcheck=1
#gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
#EOF


#yum -y install kubectl kubelet kubeadm

#systemctl enable kubelet
# 9.增加cri-dockerd配置
cd /home/centos8.2-k8s-init-binary/
cp cri-dockerd /usr/bin/
# 创建cri-docker启动文件。
cat <<EOF > /usr/lib/systemd/system/cri-docker.service
[Unit]
Description=CRI Interface for Docker Application Container Engine
Documentation=https://docs.mirantis.com
After=network-online.target firewalld.service docker.service
Wants=network-online.target
Requires=cri-docker.socket

[Service]
Type=notify
ExecStart=/usr/bin/cri-dockerd --network-plugin=cni --pod-infra-container-image=registry.aliyuncs.com/google_containers/pause:3.8
ExecReload=/bin/kill -s HUP $MAINPID
TimeoutSec=0
RestartSec=2
Restart=always

StartLimitBurst=3

StartLimitInterval=60s

LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity

TasksMax=infinity
Delegate=yes
KillMode=process

[Install]
WantedBy=multi-user.target
EOF
# 创建启动文件。
cat <<EOF > /usr/lib/systemd/system/cri-docker.socket
[Unit]
Description=CRI Docker Socket for the API
PartOf=cri-docker.service

[Socket]
ListenStream=%t/cri-dockerd.sock
SocketMode=0660
SocketUser=root
SocketGroup=docker

[Install]
WantedBy=sockets.target
EOF
# 设置开机自动启动
systemctl daemon-reload ; systemctl enable cri-docker --now

systemctl is-active cri-docker




# 为之后的创建证书创建文件夹
mkdir -p /var/lib/etcd/ && mkdir -p /data/etcd/ && chmod -R 777 /data/etcd/ && chmod -R 777 /var/lib/etcd/ && mkdir /etc/etcd/ssl -p && mkdir /etc/kubernetes/pki -p


#kubeadm init \
#--control-plane-endpoint="k8s-master" \
#--apiserver-advertise-address=192.168.1.73 \
#--image-repository registry.aliyuncs.com/google_containers \
#--kubernetes-version v1.25.2 \
#--service-cidr=10.1.0.0/16 \
#--pod-network-cidr=10.244.0.0/16 \
#--cri-socket=unix:///var/run/cri-dockerd.sock \
#--upload-certs \
#--v=5

#mkdir -p $HOME/.kube
#sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#sudo chown $(id -u):$(id -g) $HOME/.kube/config